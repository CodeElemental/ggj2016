# Details #

This is a Global Game Jam code for Bjorn - Game participant in the Global Game Jam 2016 in Skopje.
It is about a drunken Viking.

### Game Info ###

* A Windows/Mac/Linux Game.
* Version: Unversioned.


### Contribution ###

* Programming Lead         : CodeElemental (https://bitbucket.org/CodeElemental/)
* Programming              : Vidak Mijanovic (https://bitbucket.org/vidak92/) 
* 2D/3D Art + Game Design  : Tomi Dzurovski (https://www.artstation.com/artist/tomitotem)