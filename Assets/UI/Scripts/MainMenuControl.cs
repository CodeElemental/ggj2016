
using UnityEngine;
using System.Collections;

public class MainMenuControl : MonoBehaviour
{
	public GameObject LevelItemPrefab;
	public Transform LevelContainer;


	// Use this for initialization
	void Start ()
	{
		FillLevelList();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	private void FillLevelList()
	{
		Debug.LogFormat("{0} levels found", LevelScript.GetInstance().Levels.Count);

		for (int i = LevelScript.GetInstance().Levels.Count - 1 ; i >= 0; i--) 
		{
			GameObject levelUIInstance = GameObject.Instantiate<GameObject>(LevelItemPrefab);
			levelUIInstance.transform.SetParent(LevelContainer, false);

			levelUIInstance.GetComponent<LevelItem>().SetData(LevelScript.GetInstance().Levels[i]);

			levelUIInstance.GetComponent<LevelItem>().Clicked += HandleClicked;
		} 
	}

	void HandleClicked (LevelItem sender, LevelConfig data)
	{
		GameScript.GetInstance().ShowGameUI();
		LevelScript.GetInstance().StartLevel(data.Index - 1);
	}
}

