﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class BackgroundMusic : MonoBehaviour {

	[SerializeField] private AudioClip backgroundMusic;
	//private AudioSource _audio;

	private string text;
	public void TurnOFF ()
	{
		AudioListener.volume = 0;
		if (this.GetComponentInChildren <Text> ().text == "Sound ON")
			this.GetComponentInChildren <Text> ().text = "Sound OFF";
		else {
			AudioListener.volume = 1;
			this.GetComponentInChildren <Text> ().text = "Sound ON";
		}

	}

	/*void Start () {
		_audio = GetComponent<AudioSource>();
		_audio.clip = backgroundMusic;

	}	
	
	public void PlayPauseMusic()
	{

		if(_audio.isPlaying)
			_audio.Pause();
		else
			_audio.Play(); 
	}
	
	public void PlayStop()
	{
		if(_audio.isPlaying)
			_audio.Stop();
		else
			_audio.Play();
	}
	
	public void PlayMusic()
	{  
		_audio.Play();
	}
	
	public void StopMusic()
	{
		_audio.Stop();
	}
	
	public void PauseMusic()
	{
		_audio.Pause();
	}*/
}
