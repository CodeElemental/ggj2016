using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public delegate void LevelItemClickedEventHandler(LevelItem sender, LevelConfig data);

public class LevelItem : MonoBehaviour
{
	public event LevelItemClickedEventHandler Clicked;

	public Text LevelNameLabel;
	public Image LevelImage;

	private LevelConfig _data;

	public void SetData(LevelConfig config)
	{
		_data = config;
		LevelNameLabel.text = config.Index.ToString();
	}

	public void OnClick()
	{
		if (Clicked != null)
		{
			Clicked(this, _data);
		}
	}
}

