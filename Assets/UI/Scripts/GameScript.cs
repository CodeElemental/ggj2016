using UnityEngine;
using System.Collections;

public class GameScript : MonoBehaviour
{
	private static GameScript _instance;
	
	public static GameScript GetInstance()
	{
		return _instance;
	}


	public AudioSource IngameSource;
	public AudioSource MenuSource;

	public GameObject MainMenu;
	public GameObject InGameMenu;

	public GameObject VictoryPopup;
	public GameObject DefeatPopup;

	private Vector3 CamPositionMenu = new Vector3(0.4f, 0.72f, -49.1f);
	private Quaternion CamRotationMenu = Quaternion.Euler(1.7f, 180, 0);

	private Vector3 CamPositionGame = new Vector3(0,10,-15);
	private Quaternion CamRotationGame = Quaternion.Euler(30,0,0);

	void Awake()
	{
		Camera.main.orthographic = false;
		Camera.main.transform.localPosition = CamPositionMenu;
		Camera.main.transform.localRotation = CamRotationMenu;

		_instance = this;
	}
	// Use this for initialization
	void Start ()
	{
		ShowMainMenu();
	}

	public void ShowMainMenu()
	{
		MainMenu.SetActive(true);
		InGameMenu.SetActive(false);

		Camera.main.orthographic = false;
		Camera.main.transform.localPosition = CamPositionMenu;
		Camera.main.transform.localRotation = CamRotationMenu;

		IngameSource.Stop();
		MenuSource.Play();
	}

	public void ShowGameUI()
	{
		MainMenu.SetActive(false);
		InGameMenu.SetActive(true);

		Camera.main.orthographic = true;
		Camera.main.transform.localPosition = CamPositionGame;
		Camera.main.transform.localRotation = CamRotationGame;

		MenuSource.Stop();
		IngameSource.Play();
	}

	public void ShowVictory()
	{
		VictoryPopup.SetActive(true);
	}

	public void ShowDefeat()
	{
		DefeatPopup.SetActive(true);
	}

	public void QuitGame()
	{
		Application.Quit();
	}
}

