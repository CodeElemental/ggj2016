using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeScript : MonoBehaviour
{
	public Text TimeLabel; 
	
	// Update is called once per frame
	void Update ()
	{
		int minutes = (int) (LevelScript.GetInstance().TimeLeft / 60);
		int seconds = (int) (LevelScript.GetInstance().TimeLeft % 60);
		TimeLabel.text = string.Format("{0}:{1}", minutes.ToString(), seconds.ToString("D2"));
	}
}

