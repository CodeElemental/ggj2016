using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDVitals : MonoBehaviour
{
	public Image FilledImage;

	private Vitals _vitals;

	// Update is called once per frame
	void Update ()
	{
		if (_vitals != null)
		{
			FilledImage.fillAmount = (float) (_vitals.Health / _vitals.MaxHealth);
		}
	}

	public void RegisterVitals(Vitals vitals)
	{
		_vitals = vitals;
	}
}

