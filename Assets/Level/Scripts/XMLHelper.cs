using System.Xml;
using UnityEngine;
using System.Globalization;

public static class XMLHelper
{
	public readonly static NumberStyles NumberStyle = NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign;
	public readonly static CultureInfo Culture = CultureInfo.CreateSpecificCulture("en-US");

	public static string GetAttributeString(XmlNode node , string attributeName, string defaultValue)
	{
		if (node.Attributes[attributeName] != null)
			return node.Attributes[attributeName].Value;
		
		return defaultValue;
	}
	
	public static bool GetAttributeBool(XmlNode node, string attributeName, bool defaultValue)
	{
		bool result = defaultValue;
		if (node.Attributes[attributeName] != null)
		{
			bool.TryParse(node.Attributes[attributeName].Value, out result);
		}
		
		return result;
	}
	
	public static int GetAttributeInt(XmlNode node, string attributeName, int defaultValue)
	{
		int result = defaultValue;
		if (node.Attributes[attributeName] != null)
		{
			int.TryParse(node.Attributes[attributeName].Value, out result);
		}
		
		return result;
	}
	
	public static float GetAttributeFloat(XmlNode node, string attributeName, float defaultValue)
	{
		float result = defaultValue;
		if (node.Attributes[attributeName] != null)
		{
			float.TryParse(
				node.Attributes[attributeName].Value, 
				NumberStyle,
				Culture, 
				out result);
		}
		
		return result;
	}
	
	public static Vector3 GetVectorAttributes(XmlNode node)
	{
		float x = 0;float y = 0;float z = 0;
		if (node.Attributes["X"] != null &&
		    node.Attributes["Y"] != null && 
		    node.Attributes["Z"] != null)
		{
			float.TryParse(node.Attributes["X"].Value,NumberStyle, Culture, out x);
			float.TryParse(node.Attributes["Y"].Value,NumberStyle, Culture, out y);
			float.TryParse(node.Attributes["Z"].Value,NumberStyle, Culture, out z);
		}
		
		return new Vector3(x, y, z);
	}

	public static Vector2 GetVector2Attributes(XmlNode node)
	{
		float x = 0;float y = 0;
		if (node.Attributes["X"] != null &&
		    node.Attributes["Y"] != null)
		{
			float.TryParse(node.Attributes["X"].Value,NumberStyle, Culture, out x);
			float.TryParse(node.Attributes["Y"].Value,NumberStyle, Culture, out y);
		}
		
		return new Vector2(x, y);
	}
}

