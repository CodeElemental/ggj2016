
public class LevelTile
{
	public string TileName {get; set;}

	public string ObstacleName {get;set;}

	public bool IsWall { get; set; }

	public bool IsValid 
	{
		get { return !IsWall && string.IsNullOrEmpty(ObstacleName); }
	}
}