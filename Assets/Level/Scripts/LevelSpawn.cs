
using System.Collections.Generic;

public class LevelSpawn
{
	public List<string> Spawns { get; set; }

	public bool IsSpawned { get; set;}

	public LevelSpawn()
	{
		Spawns = new List<string>();
		IsSpawned = false;
	}

	public void AddSpawn(string spawnName)
	{
		Spawns.Add(spawnName);
	}
}