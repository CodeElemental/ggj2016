using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;

public class LevelScript : MonoBehaviour
{
	private static LevelScript _instance;

	public static LevelScript GetInstance()
	{
		return _instance;
	}

	public bool AutoStart = true;

	public TextAsset ConfigFile;
	public TileSpawner Spawner;
	public TowerSpawner TowerSpawner;

	public List<LevelConfig> Levels = new List<LevelConfig>();

	public GameObject PlayerPrefab;

	public float TimeLeft = 10000f;
	public bool IsPaused = false;

	public GameObject Player;

	public LevelConfig CurrentLevel;
	private int _kills = 0;

	void Awake()
	{
		_instance = this;
		InitializeFile();
	}

	void Start()
	{
		if (AutoStart)
			StartLevel(0);
	}

	void Update()
	{
		if (!IsPaused && CurrentLevel != null)
		{
			TimeLeft -= Time.deltaTime;

			if (TimeLeft < 0f)
			{
				Defeat ();
				return;
			}

			foreach (KeyValuePair<int, LevelSpawn> item in CurrentLevel.SpawnIntervals) 
			{
				if (TimeLeft <= item.Key && !item.Value.IsSpawned)
				{
					TowerSpawner.Spawn(item.Value);
					item.Value.IsSpawned = true;
					break;
				}
			}
		}
	}

	public void StartLevel(int levelIndex)
	{
		ClearLevel();

		_kills = 0;

		// Generate Tiles
		CurrentLevel = Levels[levelIndex];
		CurrentLevel.Reset();
		Spawner.SpawnLevel(CurrentLevel);

		SpawnPlayer(CurrentLevel);
		TimeLeft= CurrentLevel.TimeLimit;
		IsPaused = false;
	}

	private void InitializeFile()
	{
		XmlDocument doc = new XmlDocument();
		doc.LoadXml(ConfigFile.text);

		XmlNodeList levels = doc.SelectNodes("Levels/Level");

		for(int i = 0; i < levels.Count; i++) 
		{
			XmlNode tilesetNode = levels[i].SelectSingleNode("TileSet");

			int size = XMLHelper.GetAttributeInt(tilesetNode, "Size", 0);
			string name = XMLHelper.GetAttributeString(levels[i], "Name", "N/A");
			XmlNode spawnNode = tilesetNode.SelectSingleNode("PlayerSpawn");
			Vector2 spawnVector = XMLHelper.GetVector2Attributes(spawnNode);
			int levelIndex = XMLHelper.GetAttributeInt(levels[i],"Index", 1);

			float timeLimit = XMLHelper.GetAttributeFloat(levels[i], "TimeLimit", 60);

			LevelConfig level = new LevelConfig(size);
			level.Name = name;
			level.Index = levelIndex;
			level.TimeLimit = timeLimit;
			level.PlayerSpawn = spawnVector;

			XmlNodeList lineNodes = tilesetNode.SelectNodes("Line");

			for(int j = 0; j < lineNodes.Count; j++) 
			{
				string tileText = XMLHelper.GetAttributeString(lineNodes[j],"Value","");
				string obstaclesText = XMLHelper.GetAttributeString(lineNodes[j],"Objects","");
				Debug.Log(tileText);

				string[] rawTileData =  tileText.Split (',');
				string[] rawObstacleData =  obstaclesText.Split (',');
				if (rawTileData.Length == rawObstacleData.Length)
				{
					for (int t = 0; t < rawTileData.Length; t++) 
					{
						level.Tiles[j, t] = CreateTile(rawTileData[t], rawObstacleData[t]);
					}
				}
			}

			XmlNodeList spawnNodes = levels[i].SelectNodes("Spawns/Spawn");
			for (int j = 0; j < spawnNodes.Count; j++) 
			{
				int time = XMLHelper.GetAttributeInt(spawnNodes[j], "Time", 1);
				string spawnText = XMLHelper.GetAttributeString(spawnNodes[j], "Resources", "");

				LevelSpawn spawn = CreateLevelSpawn(spawnText);

				Debug.LogFormat("Level Spawn = {0} {1}", time.ToString(), spawnText);
				level.AddSpawn(time, spawn);
			}

			level.Initialize();
			Levels.Add(level);

			Debug.LogFormat("Level {0} initialized. Size = {1}", level.Name, level.Tiles.GetLength(0));
			Debug.LogFormat("{0} spawns in level.", level.SpawnIntervals.Count);
		}
	}

	private LevelTile CreateTile(string tileLetter, string objectLetter)
	{
		LevelTile tile = new LevelTile();
		switch (tileLetter.ToLower())
		{
		case "d": { tile.TileName="TileDirt"; break;}
		case "di": { tile.TileName="TileDirt"; break;}
		case "w": { tile.TileName="TileWall"; tile.IsWall = true; break;}
		case "g": { tile.TileName="TileGrass"; break;}
		case "gi": { tile.TileName="TileSnow"; break;}
		}

		switch (objectLetter.ToLower())
		{
		case "r": { tile.ObstacleName="Rock"; break;}
		case "t": { tile.ObstacleName="Tree"; break;}
		case "b": { tile.ObstacleName="Bush"; break;}
		case "ri": { tile.ObstacleName="RockSnow"; break;}
		case "ti": { tile.ObstacleName="TreeSnow"; break;}
		case "bi": { tile.ObstacleName="BushSnow"; break;}
		}

		return tile;
	}

	public LevelSpawn CreateLevelSpawn(string text)
	{
		string[] spawnText = text.Split(',');
		LevelSpawn spawn = new LevelSpawn();

		for (int i = 0; i < spawnText.Length; i++) 
		{
			spawn.AddSpawn(spawnText[i]);
		}

		return spawn;
	}

	private void SpawnPlayer(LevelConfig level)
	{
		Vector2 spawnPos = level.PlayerSpawn;

		int width = level.Tiles.GetLength(0);
		int length = level.Tiles.GetLength(1);

		int wOffset = (int) (width / 2);
		int hOffset = (int) (length / 2);

		Player = GameObject.Instantiate<GameObject>(PlayerPrefab);

		Player.transform.SetParent(transform, false);

		Player.transform.position = new Vector3(spawnPos.x - wOffset, 0, spawnPos.y - hOffset);

		Player.GetComponent<Player>().TowerKilled += HandleTowerKilled;
	}

	void HandleTowerKilled (global::Player sender, Tower enemy)
	{
		_kills++;

		if (_kills == CurrentLevel.TowersToKill)
		{
			Victory();
		}
	}

	public void TogglePause(bool isPaused)
	{
		IsPaused = isPaused;
	}

	public void ClearLevel()
	{
		ClearChildren(Spawner.transform);
		ClearChildren(TowerSpawner.transform);

		if (Player != null)
		{
			Player.GetComponent<Player>().TowerKilled -= HandleTowerKilled;
			GameObject.Destroy(Player);
		}
	}

	public static void ClearChildren(Transform holder)
	{
		if (Application.isEditor)
		{
			int children = holder.childCount;
			for (int i = 0; i < children; i++) 
			{
				DestroyImmediate(holder.GetChild (0).gameObject);
			}
		} else 
		{
			foreach (Transform childTransform in holder) 
			{
				Destroy(childTransform.gameObject);
			}
		}
	}


	private void Victory()
	{
		IsPaused = true;
		Player.GetComponent<Player>().StopEverything();
		Debug.Log("VICTORY");
		GameScript.GetInstance().ShowVictory();
	}

	private void Defeat()
	{
		IsPaused = true;
		Player.GetComponent<Player>().StopEverything();
		Debug.Log("DEFEAT");
		GameScript.GetInstance().ShowDefeat();
	}
}

