using UnityEngine;
using System.Collections;

public class Vitals : MonoBehaviour
{
    public float MaxHealth = 100;
    public float Health = 0;

    public HUDVitals HudVitals;


    void Start()
    {
        Health = MaxHealth;
        HudVitals.RegisterVitals(this);
    }

    public bool TakeDamage(float damage)
    {
        Health -= damage;
        Health = Mathf.Clamp(Health, 0f, MaxHealth);
        return Health <= 0;
    }

}

