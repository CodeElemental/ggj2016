
using UnityEngine;
using System.Collections.Generic;

public class LevelConfig
{
	public string Name {get;set;}
	public int Index { get; set;}

	public float TimeLimit { get; set; }
	public float TowersToKill { get; set;}

	public Vector2 PlayerSpawn;

	public LevelTile[,] Tiles;
	public float Size { get { return Tiles.GetLength(0); }}

	public Dictionary<int, LevelSpawn> SpawnIntervals = new Dictionary<int, LevelSpawn>();

	public List<Vector2> ValidPositions = new List<Vector2>();

	public LevelConfig (int size)
	{
		Tiles = new LevelTile[size,size];
	}

	public void AddSpawn(int time, LevelSpawn spawn)
	{
		SpawnIntervals.Add(time, spawn);
	}

	public void Reset()
	{
		foreach (KeyValuePair<int, LevelSpawn> item in SpawnIntervals) 
		{
			item.Value.IsSpawned = false; 
		}

		Initialize();
	}

	public void Initialize()
	{
		ValidPositions.Clear();

		int width = Tiles.GetLength(0);
		int height = Tiles.GetLength(1);

		for (int i = 0; i < width; i++) 
		{
			for (int j = 0; j < height; j++) 
			{
				if (Tiles[i,j].IsValid)
					ValidPositions.Add(new Vector2(i,j));
			}
		}

		TowersToKill = 0;

		foreach (KeyValuePair<int, LevelSpawn> item in SpawnIntervals) 
		{
			TowersToKill += item.Value.Spawns.Count; 
		}
	}

	public Vector2 ExtractRandomPosition()
	{
		int randomIndex = Random.Range(0,ValidPositions.Count);

		Vector2 result = ValidPositions[randomIndex];

		ValidPositions.RemoveAt(randomIndex);

		return result;
	}
}