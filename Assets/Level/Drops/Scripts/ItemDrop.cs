﻿using UnityEngine;
using System.Collections;

public class ItemDrop : MonoBehaviour
{
    public Transform ItemModel;
    public float RotationSpeed = 2f;

    void Update()
    {
        ItemModel.RotateAround(transform.position, Vector3.up, RotationSpeed * Time.deltaTime);
    }
}
