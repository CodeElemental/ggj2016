using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileSpawner : MonoBehaviour
{
    public Vector2 SpawnPositions = new Vector2(-5f, -10f);

    public List<GameObject> TilePrefabs = new List<GameObject>();
	public List<GameObject> ObjectPrefabs = new List<GameObject>();
	private Dictionary<string,GameObject> Tiles = new Dictionary<string, GameObject>();
	private Dictionary<string,GameObject> Objects = new Dictionary<string, GameObject>();

	void Awake()
	{
		for (int i = 0; i < TilePrefabs.Count; i++) 
		{
			Tiles.Add(TilePrefabs[i].name, TilePrefabs[i]);
		}

		for (int i = 0; i < ObjectPrefabs.Count; i++) 
		{
			Objects.Add(ObjectPrefabs[i].name, ObjectPrefabs[i]);
		}
	}

    private void SpawnTile(int width, int height, LevelTile tile)
    {
		if (Tiles.ContainsKey(tile.TileName))
		{
			float verticaloffset = Random.Range(SpawnPositions.x, SpawnPositions.y);

			GameObject tileInstance = GameObject.Instantiate<GameObject>(Tiles[tile.TileName]);
			
			tileInstance.transform.SetParent(transform, false);

			tileInstance.transform.localPosition = 
				new Vector3(
					width, 
					verticaloffset,
					height);

			if (!string.IsNullOrEmpty(tile.ObstacleName))
			{
				GameObject obstacleInstance = GameObject.Instantiate<GameObject>(Objects[tile.ObstacleName]);
				
				obstacleInstance.transform.SetParent(transform, false);
				
				obstacleInstance.transform.localPosition = 
					new Vector3(
						width, 
						-verticaloffset,
						height);
			}
		}
    }

    IEnumerator SpawnTile(int width, int height, LevelTile tile, float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        // Now do your thing here
		SpawnTile(width, height, tile);
    }

	public void SpawnLevel(LevelConfig level)
	{
		int width = level.Tiles.GetLength(0);
		int length = level.Tiles.GetLength(1);

		int wOffset = (int) (width / 2);
		int hOffset = (int) (length / 2);

		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < length; j++)
			{
				StartCoroutine(
					SpawnTile(i - wOffset, j - hOffset, level.Tiles[i,j], Random.Range(0.5f, 4f)));
			}
		}
	}
	
}

