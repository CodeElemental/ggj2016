using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour
{
	public float LerpSpeed = 4f;
	
	// Update is called once per frame
	void Update ()
	{
		if (transform.position.y > 0)
		{
			transform.position = 
				Vector3.Lerp(
					transform.position, 
					new Vector3(transform.position.x, 0, transform.position.z),
					LerpSpeed* Time.deltaTime) ;
		}
	}
}

