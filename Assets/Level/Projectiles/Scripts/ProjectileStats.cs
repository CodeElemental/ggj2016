﻿using UnityEngine;
using System.Collections;

public class ProjectileStats : MonoBehaviour
{
    public float scale, damage, speed, delay, destroyAfter;
    public string targetTag;
}
