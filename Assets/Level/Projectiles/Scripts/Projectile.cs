﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    private Rigidbody _rb;
    private ProjectileStats _stats;

    void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _stats = GetComponent<ProjectileStats>();
        Invoke("DestroyObject", _stats.destroyAfter);
        transform.localScale = Vector3.one * _stats.scale;
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("projectile hit " + other.name);
        if (tag == other.tag)
        {
            return;
        }
        if (other.tag == _stats.targetTag)
        {
            other.SendMessage("TakeDamage", _stats.damage, SendMessageOptions.DontRequireReceiver);
        }
        Destroy(gameObject);
    }

    void DestroyObject()
    {
        Destroy(gameObject);
    }

    public void SetVelocity(Vector3 vel)
    {
        _rb.velocity = vel * _stats.speed;
    }

    public void SetStats(ProjectileStats stats)
    {
        _stats = stats;
    }
}
