﻿using UnityEngine;
using System.Collections;

public class ProjectileShooter : MonoBehaviour
{
    public GameObject projectilePrefab;

    private ProjectileStats _stats;

    private float _delayTimer;
    public bool Enabled = false;

    void Awake()
    {
        _stats = GetComponent<ProjectileStats>();
        _delayTimer = 0f;
    }

    void Update()
    {
        if (Enabled && !LevelScript.GetInstance().IsPaused)
        {
            if (_delayTimer > 0f)
            {
                _delayTimer -= Time.deltaTime;
            }
        }
        
    }

    public void Shoot(Vector3 targetPosition, string tag)
    {
        if (_delayTimer > 0f)
        {
            return;
        }

        GameObject projectileInstance = Instantiate(projectilePrefab, transform.position, Quaternion.identity) as GameObject;
        projectileInstance.transform.LookAt(targetPosition);
        projectileInstance.tag = tag;
        Projectile projectileScript = projectileInstance.GetComponent<Projectile>();
        projectileScript.SetStats(_stats);
        projectileScript.SetVelocity((targetPosition - transform.position).normalized);

        _delayTimer = _stats.delay;
    }
}
