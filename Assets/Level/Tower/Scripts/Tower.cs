﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tower : MonoBehaviour
{
    public float shootingRange;

    private Vitals _vitals;
    private ProjectileShooter _projectileShooter;
    private Transform _playerTransform;

	public float SpawnSpeed = 3f;
	public Transform ModelHolder;
	private Vector3 _modelOriginalPosition;

	public GameObject HUDObject;
	public bool IsVulnerable= false;

	private ItemDropScript _dropScript;

	public GameObject HitEffect;

	private float shake = 0f;
	private float shakeAmount = 0.02f;
	private float decreaseFactor = 1f;
	private bool _isRising = true; 

	public Transform FollowPlayerModel;

    void Awake()
    {
        _vitals = GetComponent<Vitals>();
        _projectileShooter = GetComponentInChildren<ProjectileShooter>();
		_dropScript = GetComponent<ItemDropScript>();

		_modelOriginalPosition = ModelHolder.localPosition;
		_modelOriginalPosition.y = 0;

		HUDObject.SetActive(false);
		IsVulnerable = false;
    }

    void Start()
    {
        _playerTransform = LevelScript.GetInstance().Player.transform;
		shake = 1.2f;
		_isRising = true;
    }

    public bool TakeDamage(float ammount)
    {
		if (!_isRising)
		{
			Debug.LogFormat("{0} has {1} health remaining", gameObject.name, _vitals.Health.ToString());
			
			GameObject hitEffectInsance = GameObject.Instantiate<GameObject>(HitEffect);
			
			hitEffectInsance.transform.position = transform.position + new Vector3(0,1,0);
			
			if (_vitals.TakeDamage(ammount))
			{
				_dropScript.DropSomething();
				Destroy(gameObject);
				return true;
			}
			
			shake = 0.4f;
		}

		return false;
    }

    void Update()
    {
        if (!LevelScript.GetInstance().IsPaused)
        {
            if (_projectileShooter.Enabled && Vector3.Distance(transform.position, _playerTransform.position) <= shootingRange)
            {
                _projectileShooter.Shoot(_playerTransform.position, tag);
            }

			if (ModelHolder.localPosition.y < 0)
			{
				ModelHolder.localPosition = 
					Vector3.MoveTowards(
						ModelHolder.localPosition, 
						new Vector3(ModelHolder.localPosition.x, 0, ModelHolder.localPosition.z),
						SpawnSpeed * Time.deltaTime);

				if (ModelHolder.localPosition.y >= 0f)
				{
					IsVulnerable = true;
					_projectileShooter.Enabled = true;
					HUDObject.SetActive(true);
					_isRising = false;

					ModelHolder.localPosition = _modelOriginalPosition;
				}
			}

			CheckShake();

			if (!_isRising && FollowPlayerModel != null && LevelScript.GetInstance().Player != null)
			{
				FollowPlayerModel.LookAt(LevelScript.GetInstance().Player.transform);
				FollowPlayerModel.localRotation *= Quaternion.Euler(0,-90,0);
			}
        }
    }

	private void CheckShake()
	{
		if (shake > 0)
		{
			Vector3 randomVector =  Random.insideUnitSphere;
			randomVector.y = 0;

			ModelHolder.localPosition = 
				ModelHolder.localPosition + randomVector * shakeAmount;
			
			shake -= Time.deltaTime * decreaseFactor;
		}
		else
		{
			shake = 0f;
			if (!_isRising)
			{
				ModelHolder.localPosition = _modelOriginalPosition;
			}

		}
	}

}
