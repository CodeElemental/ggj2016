using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDropScript : MonoBehaviour
{
    public List<GameObject> Drops;
    public List<float> DropChances;

    public Vector3 DropOffset = Vector3.up;

    public void DropSomething()
    {
        float dropPoint = Random.Range(0f, 1f);

        float dropRange = 0f;

        for (int i = 0; i < DropChances.Count; i++)
        {
            dropRange += DropChances[i];

            if (dropPoint < dropRange)
            {
                SpawnDrop(i);
                break;
            }
        }
    }

    private void SpawnDrop(int index)
    {
        GameObject dropItem = Drops[index];

        if (dropItem != null)
        {
            GameObject dropItemInstance = GameObject.Instantiate<GameObject>(dropItem);

			dropItemInstance.transform.SetParent(transform.parent, false);
            dropItemInstance.transform.position = transform.position + DropOffset;
        }
    }
}

