using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerSpawner : MonoBehaviour
{
	public List<GameObject> TowerPrefabs = new List<GameObject>();
	private Dictionary<string,GameObject> Towers = new Dictionary<string, GameObject>();

	void Awake()
	{
		for (int i = 0; i < TowerPrefabs.Count; i++) 
		{
			Towers.Add(TowerPrefabs[i].name, TowerPrefabs[i]);
		}
	}

	public void Spawn(LevelSpawn Spawn)
	{
		Debug.LogFormat("Spawning {0} towers", Spawn.Spawns.Count);
		for (int i = 0; i < Spawn.Spawns.Count; i++) 
		{
			Vector2 spawnpos = LevelScript.GetInstance().CurrentLevel.ExtractRandomPosition();
			spawnpos -= new Vector2(
				(float)(LevelScript.GetInstance().CurrentLevel.Size / 2),
				(float)(LevelScript.GetInstance().CurrentLevel.Size / 2) );

			SpawnTower(
				spawnpos, 
				Spawn.Spawns[i]);
		}
	}

	private void SpawnTower(Vector2 position, string towerPrefab)
	{
		if (Towers.ContainsKey(towerPrefab))
		{
			GameObject towerInstance = GameObject.Instantiate<GameObject>(Towers[towerPrefab]);
			
			towerInstance.transform.SetParent(transform, false);
			
			towerInstance.transform.localPosition = 
				new Vector3(
					position.x, 
					0,
					position.y);
		}
	}
}

