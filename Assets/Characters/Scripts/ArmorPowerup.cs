﻿using UnityEngine;
using System.Collections;

public class ArmorPowerup : MonoBehaviour
{
    [SerializeField] float armorAmmount, duration;

    private float timer;
    private bool isActive;

    private Player _playerScript;

    void Awake()
    {
        _playerScript = GetComponent<Player>();
    }

    void Update()
    {
        if (!LevelScript.GetInstance().IsPaused && isActive)
        {
            timer -= Time.deltaTime;
            if (timer <= 0f)
            {
                isActive = false;
                _playerScript.ArmorDown(armorAmmount);
            }
        }
    }

    public void StartPowerup()
    {
        if (!isActive)
        {
            _playerScript.ArmorUp(armorAmmount);
        }
        isActive = true;
        timer = duration;
    }
}
