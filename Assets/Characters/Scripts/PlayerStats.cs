using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour
{
	public float MoveSpeed = 250f;
	public float Damage = 10f;
	public float AttackDelay = 0.5f;
}