﻿using UnityEngine;
using System.Collections;

public class DamagePowerup : MonoBehaviour
{
    [SerializeField]
    float damageAmmount, duration, scaleFactor;

    private float timer;
    private bool isActive;

    private Player _playerScript;

    void Awake()
    {
        _playerScript = GetComponent<Player>();
    }

    void Update()
    {
        if (!LevelScript.GetInstance().IsPaused && isActive)
        {
            timer -= Time.deltaTime;
            if (timer <= 0f)
            {
                isActive = false;
                _playerScript.DamageDown(damageAmmount);
            }
        }
    }

    public void StartPowerup()
    {
        if (!isActive)
        {
            _playerScript.DamageUp(damageAmmount, scaleFactor);
        }
        isActive = true;
        timer = duration;
    }
}
