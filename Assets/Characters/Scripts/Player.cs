﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void TowerKilledEventHandler(Player sender,Tower enemy);

public class Player : MonoBehaviour
{
    public event TowerKilledEventHandler TowerKilled;

    private Vector3 _velocity;
    private Rigidbody _rb;
    private ProjectileShooter _projectileSooter;
    private PlayerStats _playerStats;
    private Vitals _playerVitals;
    private float _attackDelay;

    private HealPowerup _healPowerup;
    private DamagePowerup _damagePowerup;
    private ArmorPowerup _armorPowerup;
    public bool _powerupColliding = false;
    private float _armor = 0f;

    public AudioSource soundFootsteps;
    public AudioSource[] soundsHit;
    public AudioSource soundPowerup;
    public AudioSource[] soundsOuch;

    public GameObject model;

    public List<GameObject> towersInRange = new List<GameObject>();

    private int LayerMaskTowers;

    private Animator _playerAnimator;
    static int _attackState = Animator.StringToHash("Attack");

    void Awake()
    {
        _playerStats = GetComponent<PlayerStats>();
        _playerVitals = GetComponent<Vitals>();

        _rb = GetComponent<Rigidbody>();
        _projectileSooter = GetComponentInChildren<ProjectileShooter>();
        _playerAnimator = GetComponentInChildren<Animator>();

        _healPowerup = GetComponent<HealPowerup>();
        _damagePowerup = GetComponent<DamagePowerup>();
        _armorPowerup = GetComponent<ArmorPowerup>();

        LayerMaskTowers = 1 << LayerMask.NameToLayer("Tower");
    }

    void Start()
    {
        _velocity = Vector3.zero;
		
        _playerAnimator.SetFloat("AttackSpeed", 8f);
        _playerAnimator.SetFloat("MoveSpeed", 1.5f);

    }

    void Update()
    {
        if (!LevelScript.GetInstance().IsPaused)
        {
            // Get velocity from input
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            _velocity.x = x;
            _velocity.z = z;
			
            _playerAnimator.SetBool("IsWalking", _velocity.magnitude > 0);
            model.transform.LookAt(transform.position + _velocity.normalized * 5f);

            if (Mathf.Abs(_velocity.x) > 0f || Mathf.Abs(_velocity.z) > 0f)
            {
                if (!soundFootsteps.isPlaying)
                {
                    soundFootsteps.Play();
                }
            }
            else
            {
                soundFootsteps.Stop();
            }

			
            // handle attack if able.
            if (_attackDelay > 0)
            {
                _attackDelay -= Time.deltaTime;
            }
            else
            {
                // Trigger detection.
                if (Input.GetMouseButtonDown(0))
                { // left click
                    if (!_playerAnimator.GetBool("IsAttacking"))
                    {
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
						
                        RaycastHit hit;
						
                        if (Physics.Raycast(ray, out hit, 100f, LayerMaskTowers))
                        {
                            if (towersInRange.Contains(hit.collider.gameObject))
                            {
                                if (hit.collider.gameObject.GetComponent<Tower>().TakeDamage(_playerStats.Damage))
                                {
                                    MakeKill(hit.collider.gameObject.GetComponent<Tower>());
                                }
								
                                _playerAnimator.SetTrigger("MakeAttack");
                                _playerAnimator.SetBool("IsAttacking", true);

                                soundsHit[Random.Range(0, 2)].Play();
                            }
                            else
                            {
                                Debug.LogFormat("cant hit {0}. not in range.", hit.collider.gameObject);
                            }
                        }
                    }
                }
            }
        }
    }

    private void MakeKill(Tower tower)
    {
        if (TowerKilled != null)
        {
            TowerKilled(this, tower);
        }
    }

    void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.tag == "Tower")
        {
            towersInRange.Add(other.gameObject);
            Debug.LogFormat("{0} in range ", other.gameObject.name);
        }
		else if (other.gameObject.tag == "DropBlue" 
		         || other.gameObject.tag == "DropWhite" 
		         || other.gameObject.tag == "DropYellow")
        {
//            if (_powerupColliding)
//            {
//				_powerupColliding = false;
//                return;
//            }

            Destroy(other.gameObject);
            //_powerupColliding = true;
            soundPowerup.Play();
            Debug.Log(other.tag + " blue picked up");

            if (other.gameObject.tag == "DropBlue")
            {
                Heal();
            }
            else if (other.gameObject.tag == "DropYellow")
            {
                _damagePowerup.StartPowerup();
            }
            else if (other.gameObject.tag == "DropWhite")
            {
                _armorPowerup.StartPowerup();
            }

            //Invoke("EnablePowerupColliding", 0.1f);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Tower")
        {
            if (towersInRange.Contains(other.gameObject))
            {
                towersInRange.Remove(other.gameObject);
                Debug.LogFormat("{0} out of range ", other.gameObject.name);
            }
            else
            {
                Debug.LogFormat("{0} error ");
            }   
        }
    }

    void FixedUpdate()
    {
        _rb.velocity = _velocity.normalized * _playerStats.MoveSpeed * Time.deltaTime;
    }

    public void TakeDamage(float ammount)
    {
        _playerVitals.TakeDamage(ammount - _armor);
//		if (soundsOuch.Length > 0)
//			soundsOuch[0].Play();
    }

    void Heal()
    {
        _playerVitals.Health += _healPowerup.healAmmount;
        _playerVitals.Health = Mathf.Min(_playerVitals.MaxHealth, _playerVitals.Health);
    }

    public void StopEverything()
    {
        _velocity = Vector2.zero;
        _playerAnimator.SetBool("IsRunning", false);
        _playerAnimator.SetBool("IsAttacking", false);
    }

    public void DamageUp(float ammount, float scaleFactor)
    {
        _playerStats.Damage += ammount;
        transform.localScale = Vector3.one * scaleFactor;
    }

    public void DamageDown(float ammount)
    {
        _playerStats.Damage -= ammount;
        transform.localScale = Vector3.one;
    }

    public void ArmorUp(float ammount)
    {
        _armor += ammount;
    }

    public void ArmorDown(float ammount)
    {
        _armor -= ammount;
    }

    public void EnablePowerupColliding()
    {
        _powerupColliding = false;
    }
}
